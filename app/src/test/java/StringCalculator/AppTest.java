package StringCalculator;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;


public class AppTest {
    @Test
    public void testEmptyStringReturnsZero() {

        int expected = 0;
        int actual = StringCalculator.add("");

        org.junit.Assert.assertEquals(".add should return zero when called on an empty string", expected, actual);

    }

    @Test
    public void testSingleNumberReturnsItselfAsAnInteger(){
        int expected = 8;
        int actual = StringCalculator.add("8");

        assertEquals(".add should return the same number as an integer when called on a single number", expected, actual);
    }

    @Test
    public void testAddOneAndTwo(){
        int expected = 3;
        int actual = StringCalculator.add("1,2");

        assertEquals(".add adds 1 and 2 successfully", expected, actual);
    }

    @Test
    public void testHandleNewLineCharacters(){
        int expected = 3;
        int actual = StringCalculator.add("1\n2");

        assertEquals(".add adds 1 and 2 successfully when delineated by a new line", expected, actual);
    }

    @Test
    public void testSupportForSpecifiedSemicolonDelimiter(){
        int expected = 3;
        int actual = StringCalculator.add("//;\n1;2");

        assertEquals(".add successfully adds 1 and 2 when provided a user-specified delimiter", expected, actual);
    }

    @Test
    public void testThrowExceptionForNegativeNumber(){
        Exception exception = assertThrows(Exception.class, () -> {
            StringCalculator.add("-2");
        });
    }
}
