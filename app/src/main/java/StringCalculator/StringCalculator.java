package StringCalculator;

import java.util.Arrays;

public class StringCalculator {

    public static int add (String input) throws Exception {
        int result = 0;
        String[] split;
        if (input.length() == 1) {
            return Integer.parseInt(input);
        } else if (input.length() > 1) {
            String delimiter = "";
            if (input.startsWith("//")) {
                delimiter = input.substring(2,input.indexOf("\n"));
                split = input.split("[" + delimiter + "|\n]");
            } else {
                split = input.split("[,|\n]");
            }

            for (int i = 0; i < split.length; i++) {
                if (!delimiter.equals("")) {
                    split[0] = "0";
                    split[1] = "0";
                }
                result += Integer.parseInt(split[i]);
            }
        }
        return result;
    }

}